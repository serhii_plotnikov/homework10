<?php

namespace Tests\Unit;

use App\Entities\Product;
use App\Exceptions\ProductNotFoundException;
use App\Exceptions\UserNotHasProducts;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Services\MarketService;
use Illuminate\Http\Request;
use Tests\TestCase;

class ProductTest extends TestCase
{
    private $marketService;
    /**
     * @var \Mockery\MockInterface $productRepositoryMock
     */
    private $productRepositoryMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->productRepositoryMock = \Mockery::mock(ProductRepositoryInterface::class);
        $this->instance(ProductRepositoryInterface::class, $this->productRepositoryMock);
        $this->marketService = $this->app->make(MarketService::class);
    }

    public function testGetProductList()
    {
        $this->productRepositoryMock->shouldReceive('findAll')
            ->andReturn(factory(Product::class, 5)->make());
        $products = $this->marketService->getProductList();
        $this->assertCount(5, $products);
        $this->assertContainsOnlyInstancesOf(Product::class, $products);
    }

    public function testGetProductById()
    {
        $this->productRepositoryMock->shouldReceive()->findById(1)
            ->andReturn(factory(Product::class)->make(['id' => 1]));
        $product = $this->marketService->getProductById(1);
        $this->assertNotNull($product);
        $this->assertEquals(1, $product->id);
    }

    public function testGetProductByIdFail()
    {
        $this->productRepositoryMock->shouldReceive()->findById(1)
            ->andReturnNull();
        $this->expectException(ProductNotFoundException::class);
        $this->marketService->getProductById(1);
    }

    public function testGetProductsByUserId()
    {
        $this->productRepositoryMock->shouldReceive('findByUserId')
            ->andReturn(factory(Product::class, 3)->make()->each(function (Product $product) {
                static $i = 1;
                $product->id = $i++;
                $product->user_id = 1;
            }));
        $products = $this->marketService->getProductsByUserId(1);
        $this->assertCount(3, $products);
        foreach ($products as $product) {
            $this->assertEquals(1, $product->user_id);
        }
    }

    public function testGetProductsByUserIdFail()
    {
        $this->productRepositoryMock->shouldReceive('findByUserId')
            ->andReturnNull();
        $this->expectException(UserNotHasProducts::class);
        $this->marketService->getProductsByUserId(1);
    }

    public function testStoreProduct()
    {
        $requestParams = [
            'user_id' => 1,
            'name' => 'Samsung A50"',
            'price' => 450
        ];
        $requestMock = $this->getMockBuilder(Request::class)->disableOriginalConstructor()
            ->setMethods(['input', 'user'])->getMock();

        $requestMock->expects($this->at(0))
            ->method('user')
            ->willReturnCallback(function () use ($requestParams) {
                return new class($requestParams)
                {
                    public $id;

                    public function __construct(array $requsetParams)
                    {
                        $this->id = $requsetParams['user_id'];
                    }
                };
            });

        $requestMock->expects($this->at(1))
            ->method('input')
            ->with('product_name')
            ->willReturn($requestParams['name']
            );
        $requestMock->expects($this->at(2))
            ->method('input')
            ->with('product_price')
            ->willReturn($requestParams['price']);


        $this->productRepositoryMock->shouldReceive('store')->andReturnUsing(function ($params) {
            return $params;
        });
        $savedProduct = $this->marketService->storeProduct($requestMock);

        $this->assertNotNull($savedProduct);
        $this->assertEquals($requestParams['name'], $savedProduct->name);
        $this->assertEquals($requestParams['user_id'], $savedProduct->user_id);
        $this->assertEquals($requestParams['price'], $savedProduct->price);
    }

}
