<?php


namespace Tests\Feature;


use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    const ENDPOINT = '/api/items/';

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    public function testShowList()
    {
        $response = $this->get(self::ENDPOINT);
        $response->assertStatus(200);
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
        $this->assertCount(6, $response->getOriginalContent());
    }

    public function testShowProduct()
    {
        $fragment = [
            'data' => [
                'id',
                'name',
                'price',
                'user_id'
            ]
        ];
        $response = $this->get(self::ENDPOINT . '1');
        $response->assertStatus(200);
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
        $response->assertJsonStructure($fragment);
    }

    public function testShowProductNotExistingId()
    {
        $response = $this->get(self::ENDPOINT . '100000');
        $response->assertStatus(400);
        $response->assertJsonStructure(['result', 'message']);
    }

    public function testStore()
    {
        $user = factory(User::class)->create();

        $product = [
            'product_name' => 'Samsung A50',
            'product_price' => 450
        ];
        $response = $this->actingAs($user)->post(self::ENDPOINT, $product);
        $response->assertStatus(201);
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
        $this->assertDatabaseHas('products', [
            'name' => $product['product_name'],
            'price' => $product['product_price'],
            'user_id' => $user->id
        ]);
    }

    public function testDelete()
    {
        $user = User::find(1);
        $params = ['id' => 1];
        $response =  $this->actingAs($user)->delete(self::ENDPOINT . $params['id'], $params);
        $response->assertStatus(204);
        $this->assertDatabaseMissing('products', ['id' => $params['id']]);
    }

    public function testDeleteOtherProduct()
    {
        $params = ['id' => 6];
        $user = User::find(1);
        $response = $this->actingAs($user)->delete(self::ENDPOINT . $params['id'], $params);
        $response->assertStatus(403);
    }

    public function testDeleteNotExistingProduct()
    {
        $params = ['id' => 1000];
        $response = $this->delete(self::ENDPOINT . $params['id'], $params);
        $response->assertStatus(400);
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
        $this->assertDatabaseMissing('products', ['id' => $params['id']]);
        $response->assertJsonStructure(['result', 'message']);
    }
}