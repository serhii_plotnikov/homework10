<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'price' => $faker->randomFloat(2, 1, 100),
        'user_id' => $faker->numberBetween(1,10),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
