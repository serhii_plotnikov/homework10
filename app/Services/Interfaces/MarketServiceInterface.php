<?php

namespace App\Services\Interfaces;

use App\Entities\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

interface MarketServiceInterface
{
    public function getProductList(): Collection;

    public function getProductById(int $id): ?Product;

    public function getProductsByUserId(int $userId): Collection;

    public function storeProduct(Request $request): Product;

    public function deleteProduct(Request $request): void;

}