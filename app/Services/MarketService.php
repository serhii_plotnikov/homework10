<?php

namespace App\Services;

use App\Entities\Product;
use App\Exceptions\DeniedProductPermission;
use App\Exceptions\ProductNotFoundException;
use App\Exceptions\UserNotHasProducts;
use App\Services\Interfaces\MarketServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\ProductRepositoryInterface as ProductRepository;

class MarketService implements MarketServiceInterface
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getProductList(): Collection
    {
        return $this->productRepository->findAll();
    }

    public function getProductById(int $id): ?Product
    {
        $product = $this->productRepository->findById($id);

        if (!$product) {
            throw new ProductNotFoundException("No product with id: {$id}");
        }

        return $product;
    }

    public function getProductsByUserId(int $userId): Collection
    {
        $products = $this->productRepository->findByUserId($userId);
        if (!$products) {
            throw new UserNotHasProducts("User doesn't have products");
        }
        return $products;
    }

    public function storeProduct(Request $request): Product
    {
        $product = new Product([
            'user_id' => $request->user()->id,
            'name' => $request->input('product_name'),
            'price' => $request->input('product_price')
        ]);

        return $this->productRepository->store($product);
    }

    public function deleteProduct(Request $request): void
    {
        $product = $this->getProductById($request->id);
        if (!$product) {
            throw new ProductNotFoundException("No product with id: $request->id");
        } elseif ($product->user_id !== $request->user()->id) {
            throw new DeniedProductPermission("It's not your project. Access denied");
        }
        $this->productRepository->delete($product);
    }
}
