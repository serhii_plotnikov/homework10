<?php

namespace App\Policies;

use App\User;
use App\Entities\Product;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ProductPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Product $product)
    {
        return Auth::check() && $user->id === $product->user_id;
    }


}
