<?php

namespace App\Http\Controllers;

use App\Exceptions\DeniedProductPermission;
use App\Exceptions\ProductNotFoundException;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList()
    {
        $products = $this->marketService->getProductList();
        return ProductResource::collection($products);
    }

    public function store(Request $request)
    {
        $product = $this->marketService->storeProduct($request);
        return (new ProductResource($product))->response()->setStatusCode(201);
    }

    public function showProduct(int $id)
    {
        try {
            $product = $this->marketService->getProductById($id);
        } catch (ProductNotFoundException $exception) {
            return response()->json([
                'result' => 'fail',
                'message' => $exception->getMessage()
            ], 400);
        }
        return new ProductResource($product);
    }

    public function delete(Request $request)
    {
        try {
            $this->marketService->deleteProduct($request);
        } catch (ProductNotFoundException $exception) {
            return response()->json([
                'result' => 'fail',
                'message' => $exception->getMessage()
            ], 400);
        } catch (DeniedProductPermission $exception) {
            return response()->json([
                'result' => 'fail',
                'message' => $exception->getMessage()
            ], 403);
        }
        return response()->json([
            null
        ], 204);
    }
}
