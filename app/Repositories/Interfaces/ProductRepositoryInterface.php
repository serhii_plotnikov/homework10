<?php


namespace App\Repositories\Interfaces;


use App\Entities\Product;
use Illuminate\Database\Eloquent\Collection;

interface ProductRepositoryInterface
{
    public function findAll();

    public function findById(int $id): ?Product;

    public function findByUserId(int $userId): ?Collection;

    public function store(Product $product): Product;

    public function delete(Product $product): void;
}